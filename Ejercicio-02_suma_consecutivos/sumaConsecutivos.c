 Nombre: sumaConsecutivos.c
  Autor: Arianna Jatiziri Ortega Hernández
  Fecha: 12/08/2020 
  
#include  <stdio.h>

int main(void)
{
    int cuantos, num, suma, i;
    printf("Suma del conjunto de números que elijas\n");
    printf("Cuántos quieres?");
    scanf(" %d", &cuantos);
    for (i=0; i<cuantos; i++)
    {
        printf("\nTeclea el número: ");
        scanf(" %d", &num);
        suma = suma + num;
    }
    printf("\n La suma vale: %d", suma);
    return 0;
}