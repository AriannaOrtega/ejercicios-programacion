/*Programa que calcula el perímetro de un triángulo equilátero.
 Autor:Arianna Ortega
 Fecha:13/08/2020 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  float perimetro,lado1,lado2,lado3;

  //Triángulo equilátero

  printf("\n Perimetro de un triangulo \n");
  printf("\n Dame la medida de sus tres lados: ");
  scanf("%f%f%f",&lado1,&lado2,&lado3);

  perimetro=(lado1+lado2+lado3);

  printf("\n El perimetro del triangulo equilátero es: %.3f",perimetro);

	getchar();
    return 0;
}