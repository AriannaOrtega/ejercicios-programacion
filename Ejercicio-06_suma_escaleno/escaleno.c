/*Programa que calcula el perímetro de un triángulo escaleno.
 Autor:Arianna Ortega
 Fecha:13/08/2020 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  float perimetro,base,altura,hipotenusa;

  //Triángulo escaleno

  printf("\n Perimetro de un triangulo escaleno \n");
  printf("\n Dame la altura, la base y la hipotenusa:");
  scanf("%f%f%f",&base,&altura,&hipotenusa);

  perimetro=(base+altura+hipotenusa);

  printf("\n El perimetro del triangulo escaleno es: %.3f",perimetro);

	getchar();
    return 0;
}