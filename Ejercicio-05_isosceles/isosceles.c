/*Programa que calcula el perímetro de un triángulo isosceles.
 Autor:Arianna Ortega
 Fecha:13/08/2020 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  float perimetro,base,lado1,lado2;

  //Triángulo isosceles

  printf("\n Perimetro de un triangulo \n");
  printf("\n Dame la base y los dos lados: ");
  scanf("%f%f%f",&base,&lado1,&lado2);

  perimetro=(base+lado1+lado2);

  printf("\n El perimetro del triangulo isosceles es: %.3f",perimetro);

	getchar();
    return 0;
}